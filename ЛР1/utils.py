import time
import numpy as np
import copy
import random


def vector_add(v, w):
    """adds two vectors componentwise"""
    return [v_i + w_i for v_i, w_i in zip(v,w)]


def vector_subtract(v, w):
    """subtracts two vectors componentwise"""
    return [v_i - w_i for v_i, w_i in zip(v,w)]


def scalar_multiply(c, v):
    """scalar multiply a number by a vector"""
    return [c * v_i for v_i in v]


def vector_sum(vectors):
    """adds n vectors componentwise"""
    result = vectors[0]
    for vector in vectors[1:]:
        result = vector_add(result, vector)
    return result


def dot(v, w):
    """v_1 * w_1 + ... + v_n * w_n"""
    return sum(v_i * w_i for v_i, w_i in zip(v, w))


def sum_of_squares(v):
    """v_1 * v_1 + ... + v_n * v_n"""
    return dot(v, v)


def squared_distance(v, w):
    return sum_of_squares(vector_subtract(v, w))


def distance(v, w):
    return np.sqrt(squared_distance(v, w))


def vector_mean(vectors):
    """
    compute the vector whose i-th element is the mean of the
    i-th elements of the input vectors
    """
    n = len(vectors)
    return scalar_multiply(1/n, vector_sum(vectors))


def timer(func):
    def f(*args, **kwargs):
        before = time.time()
        rv = func(*args, **kwargs)
        after = time.time()
        print("Function '{0}' is done. Elapsed: {1:.2f} sec.".format(func.__name__, after - before))
        return rv

    return f