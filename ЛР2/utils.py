import time
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.neighbors import NearestNeighbors
from sklearn.metrics import homogeneity_score, completeness_score, \
    v_measure_score, adjusted_rand_score, adjusted_mutual_info_score, \
    silhouette_score


def timer(func):
    def f(*args, **kwargs):
        before = time.time()
        rv = func(*args, **kwargs)
        after = time.time()
        print("Function '{0}' is done. Elapsed: {1:.2f} sec.".format(func.__name__, after - before))
        return rv

    return f


def show_scatter_plot(x, labels_list, s1=50, title_text='Result clusterization'):
    data = pd.DataFrame(x, columns=['x', 'y'])
    data['n_cluster'] = labels_list + 1
    plt.title(title_text)
    sns.scatterplot(data=data, x='x', y='y', hue='n_cluster', palette="Set1", s=s1, legend='full')
    plt.legend(bbox_to_anchor=(1, 1))
    plt.show();


def calculate_metrics(n_clusters,
                      n_noise,
                      labels_true,
                      labels,
                      X=None,
                      return_dataframe=False):
    homogeneity_score_ = homogeneity_score(labels_true, labels)
    completeness_score_ = completeness_score(labels_true, labels)
    v_measure_score_ = v_measure_score(labels_true, labels)
    adjusted_rand_score_ = adjusted_rand_score(labels_true, labels)
    adjusted_mutual_info_score_ = adjusted_mutual_info_score(labels_true, labels)
    if X is not None:
        silhouette_score_ = silhouette_score(X, labels)
    else:
        silhouette_score_ = np.NaN

    print("Estimated number of clusters: %d" % n_clusters)
    print("Estimated number of noise points: %d" % n_noise)
    print("Homogeneity: %0.3f" % homogeneity_score_)
    print("Completeness: %0.3f" % completeness_score_)
    print("V-measure: %0.3f" % v_measure_score_)
    print("Adjusted Rand Index: %0.3f" % adjusted_rand_score_)
    print("Adjusted Mutual Information: %0.3f"% adjusted_mutual_info_score_)
    if X is not None:
        print("Silhouette Coefficient: %0.3f" % silhouette_score_)
    if return_dataframe:
        return pd.DataFrame(
            data = {
                'homogeneity': [homogeneity_score_],
                'completeness_score': [completeness_score_],
                'v_measure_score': [v_measure_score_],
                'adjusted_rand_score': [adjusted_rand_score_],
                'adjusted_mutual_info_score': [adjusted_mutual_info_score_],
                'silhouette_score': [silhouette_score_]
            }
        )
        

def plot_epsilon_research(x, y1, y2):
    neigh = NearestNeighbors(n_neighbors=2)
    neigh = neigh.fit(x)
    distances, indices = neigh.kneighbors(x)
    distances = np.sort(distances, axis=0)
    distances = distances[:,1]
    fig, ax = plt.subplots()
    if y1 is not None and y2 is not None:
        ax.fill_between([i for i in range(len(distances))], y1, y2, facecolor='orange')
    plt.plot(distances)
    plt.show()


class DBSCAN:
    """performs DBSCAN clustering"""
    def __init__(self, eps, min_pts, distance):
        self.k_ = None           # number of clusters
        self.clusters_ = None
        self.noise_clusters_ = None
        self.eps = eps           # eps for clusters
        self.min_pts = min_pts   # min counts of neighbours
        self.distance = distance # distance function

    @timer
    def fit(self, inputs):
        """fit DBSCAN"""
        def search_neighbours(point):
            return [neighbour for neighbour in inputs
                    if self.distance(point, neighbour) < self.eps]
        
        NOISE_ = 0
        num_claster = 0

        visited_points = set() 
        clustered_points = set()
        noise_set = set()
        clusters = {}
 
        for point in inputs:
            if point in visited_points:
                continue
            visited_points.add(point)
            # Находим точки в ε окрестности каждой точки
            neighbours = search_neighbours(point)
            # Выделяем основные точки с более чем minPts соседями
            if len(neighbours) < self.min_pts:
                noise_set.add(point)
            else:
                num_claster += 1
                if num_claster not in clusters:
                    clusters[num_claster] = []
                clusters[num_claster].append(point)
                clustered_points.add(point)
                # ищем соседей соседей и т.д.
                while neighbours:
                    neighbour = neighbours.pop()
                    if neighbour not in visited_points:
                        visited_points.add(neighbour)
                        neighbors_neighbors = search_neighbours(neighbour)
                        # если соседей у соседей больше, чем min_pts, то расширяем список соседей
                        if len(neighbors_neighbors) > self.min_pts:
                            neighbours.extend(neighbors_neighbors) # добавить именно элементы списка а не сам список
                    if neighbour not in clustered_points:
                        clustered_points.add(neighbour)
                        clusters[num_claster].append(neighbour)
                        if neighbour in noise_set:
                            noise_set.remove(neighbour)

        self.k_ = num_claster
        self.clusters_ = clusters
        self.noise_clusters_ = noise_set
